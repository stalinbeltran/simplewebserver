var app = angular.module('webserver', [])

app.controller('webservice', function ($scope, $http) {
  // Some values to start with
  $scope.value1 = '7'
  $scope.value2 = '2'

  // function to get the server generated values
  function multiply () {
    // Stringify input values for easy sending (and receiving)
    var parameter = JSON.stringify({value1: $scope.value1, value2: $scope.value2})
    $http.post('http://localhost:8080/multiply', parameter).success(function (data) {
      $scope.output = data
    })
  }

  $scope.multiply = multiply
  // Multiply default values the first time
  $scope.multiply()
})
