var http = require('http')
var fs = require('fs')
// Create a server
var server = http.createServer(handleRequest)
// port
var PORT = 8080

// Multiply input values, and send error if not a number
function multiply (value1String, value2String) {
  // set our output by default
  var output = {result: '', error: ''}

  // Get the numeric values of our inputs
  var value1 = parseFloat(value1String)
  var value2 = parseFloat(value2String)

  // Are those values not a number?
  if (typeof value1 !== 'number' || isNaN(value1)) {
    output.error = 'Value -' + value1String + '- is not a number. Please write a number. '
    return JSON.stringify(output)
  }
  if (typeof value2 !== 'number' || isNaN(value2)) {
    output.error += 'Value -' + value2String + '- is not a number. Please write a number.'
    return JSON.stringify(output)
  }

  // do the actual product only if everything is OK
  output.result = value1 * value2
  return JSON.stringify(output)
}

// Process a multiply request
function processMultiply (request, response) {
  var body = []
  request.on('data', function (chunk) {
    // save the chunks for later
    body.push(chunk)
  }).on('end', function () {
    // all those chunks to a single string
    body = Buffer.concat(body).toString()
    var values = JSON.parse(body)

    // time to write our response = value1*value2
    response.end(multiply(values.value1, values.value2))
  })
}

// write a simple file server, to serve every file requested
function fileServer (url, response) {
  var path = ''
  try {
    // Let's make this url relative to our directory
    path = '.' + url
    // read requested path
    fs.readFile(path, 'utf8', function (err, data) {
      if (err) {
        response.end('There was an error while reading the file "' + path + '":' + err)
        console.log(err)
        return
      }
      // if we got the data, return this data to our client
      response.end(data)
    })
  } catch (err) {
    console.log(err)
  }
}

// function to handle basically two kinds of requests: a 'multiply' request, and a 'default' file request
function handleRequest (request, response) {
  console.log(request.url)
  switch (request.url) {
    case '/multiply':
      processMultiply(request, response)
      break

    default:
      fileServer(request.url, response)
      break
  }
}

// Lets start our server
server.listen(PORT, function () {
  console.log('Server listening on: http://localhost:%s', PORT)
})
